document.addEventListener(`keypress`, (event) => {
    const buttons = document.querySelectorAll(".btn");

    buttons.forEach((btn) => {
        if (event.code === `Key${btn.innerText}` || event.code === `${btn.innerText}`) {
            btn.classList.add("highlighted");
        } else if (btn.classList.contains("highlighted")) {
            btn.classList.remove("highlighted");
        }
    });
});
